# VLC 4.x design

## Layout

- Navigation on the top: [layoutTop.png](pictures/layout/layoutTop.png)

- Navigation on the left:
    - [layoutLeftB.png](pictures/layout/layoutLeftB.png)
    - [layoutLeftC.png](pictures/layout/layoutLeftC.png)
    - [layoutLeftD.png](pictures/layout/layoutLeftD.png)

## Seekbar

- Chapters and bookmarks integrated:
    - [seekA.png](pictures/seekbar/seekA.png)
    - [seekB.png](pictures/seekbar/seekB.png)
    - [seekC.png](pictures/seekbar/seekC.png)

- Chapters integrated and bookmarks on the side: [seekD.png](pictures/seekbar/seekD.png)

- Chapters and bookmarks on the side: [seekE.png](pictures/seekbar/seekE.png)

## Classic theme

- Top bar
    - With the menu bar: [topBarA.png](pictures/classic/topBarA.png)
    - Without the menu bar: [topBarB.png](pictures/classic/topBarB.png)

- Tiny top bar
    - With the menu bar:
        - [topBarC.png](pictures/classic/topBarC.png)
        - [topBarD.png](pictures/classic/topBarD.png)
